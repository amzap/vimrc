# This is a simple makefile to get my vim up and running

current_dir := $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))


.PHONY=all

all:
	ln -fs $(current_dir)/.vimrc ${HOME}/.vimrc
	cp -a .vim ${HOME}
	git clone https://github.com/gmarik/vundle.git ~/.vim/bundle/vundle

	
