"" Adrian Moreno's vimscript. Ideally this file will be inside my git
"" repository and symlinked to ~/.vimrc
"" NOTE: This configuration depends on vundle. If it's a new system, run:
"" git clone https://github.com/gmarik/vundle.git ~/.vim/bundle/vundle

"" Vundle specifics {{
filetype off                  " required!

set rtp+=~/.vim/bundle/vundle/
call vundle#rc()

" Leader map. Has to be defined before plugins are loaded!
let mapleader = ","

" let Vundle manage Vundle
" " required!
" Bundle 'gmarik/vundle'
Bundle 'gmarik/vundle'

Bundle 'DirDiff.vim'

"}} Vundle specifics

"" Vundle bundles {{

Bundle 'OmniCppComplete'
" build tags of your own project with Ctrl-F12
map <C-F12> :!ctags -R --sort=yes --c++-kinds=+p --fields=+iaS --extra=+q .<CR>

set tags=./tags; "Recursively look for tags
"Commonly used tags: stl, libc...
set tags+=$HOME/.vim/cpp_tags
set tags+=$HOME/.vim/asio_tags
set path=.,,,** "Path is buffer path, current path (<**> means recursive)

" OmniCppComplete
let OmniCpp_NamespaceSearch = 1
let OmniCpp_GlobalScopeSearch = 1
let OmniCpp_ShowAccess = 1
let OmniCpp_ShowPrototypeInAbbr = 1 " show function parameters
let OmniCpp_MayCompleteDot = 1 " autocomplete after .
let OmniCpp_MayCompleteArrow = 1 " autocomplete after ->
let OmniCpp_MayCompleteScope = 1 " autocomplete after ::
let OmniCpp_DefaultNamespaces = ["std", "_GLIBCXX_STD"]
" automatically open and close the popup menu / preview window
au CursorMovedI,InsertLeave * if pumvisible() == 0|silent! pclose|endif
set completeopt=menuone,menu,longest,preview
"""Bundle 'rdnetto/YCM-Generator'
"""Bundle 'Valloric/YouCompleteMe'

" Tagbar {
Bundle 'majutsushi/tagbar'
map <F4> :TagbarToggle<CR>
" }Tagbar
Bundle 'mbbill/undotree'
map <Leader>u :UndotreeToggle<CR>
" CtrP {
Bundle 'kien/ctrlp.vim'
map <Leader>b :CtrlPBuffer<CR>
map <Leader>w :CtrlPCurWD<CR>
"} CtrlP

Bundle 'scrooloose/nerdtree'
map <F7> :NERDTreeFind<CR>

Bundle 'flazz/vim-colorschemes'

Bundle 'davidhalter/jedi-vim'

Bundle 'fugitive.vim'

""Syntastic configs
Bundle 'scrooloose/syntastic'

set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 0
let g:syntastic_check_on_wq = 0
let g:syntastic_python_flake8_args='--disable=E501'

"" Truescript syntax
Bundle 'leafgarland/typescript-vim'

Bundle 'vim-airline/vim-airline'
Bundle 'vim-airline/vim-airline-themes'
" To display the status line always
set laststatus=2

Bundle 'tpope/vim-surround'


Bundle 'Conque-GDB'
let g:ConqueGdb_Leader = '-'
"" }} Vundle bundles
"" General {{

Bundle 'SirVer/ultisnips'

 " Snippets are separated from the engine. Add this if you want them:
Bundle 'honza/vim-snippets'

" Trigger configuration. Do not use <tab> if you use
let g:UltiSnipsExpandTrigger="<Leader>s"
let g:UltiSnipsJumpForwardTrigger="<Leader>d"
let g:UltiSnipsJumpBackwardTrigger="<Leader>a"

 " If you want :UltiSnipsEdit to split your window.
 let g:UltiSnipsEditSplit="vertical"

filetype plugin indent on    " Also required for Vundle

" Read files when modified externally
set autoread

" Man pages in Vim!
runtime! ftplugin/man.vim

" C-s to save
:nmap <C-s> :update<CR>
:imap <C-s> <Esc>:update<CR>


"set autochdir "Still deciding if I like this...

"" }} General

"" Visual settings{{
:syn on

" Show lines
set number
set ruler

" Ignore case when searching
set ignorecase

" When searching try to be smart about cases
set smartcase

" Highlight search results
set hlsearch
map <F3> :set invhlsearch<CR>


" Try persistent undo
try
    execute ':silent !mkdir -p ~/.vim/.undodir'
    set undodir=$HOME/.vim/.undodir
    set undofile
    set undolevels=500
catch
endtry

" Work-around incomplete terminfo databases
" Particulalry useful when under `screen`, which may or may not be attached to
" a physical terminal capable of 256color mode.
if match($TERMCAP, 'Co#256:') == 0 || match($TERMCAP, ':Co#256:') > 0
    set t_Co=256
endif

let simplecolor=$AM_VIM_SIMPLECOLOR
if simplecolor == '1'
    colorscheme desert
else
    colorscheme jellybeans
endif

if &diff
        colorscheme jellybeans
endif
"set background=dark


"Tabulation and indentation
set softtabstop=4
set tabstop=8
set autoindent
set shiftwidth=4
set smarttab
set smartindent
set expandtab

set number
set notextmode
set hlsearch
set cino=(0

"Pmenu colors: the default ones are horrible!
""highlight Pmenu ctermbg=blue ctermfg=white
""hi PmenuSel ctermbg=yellow ctermfg=black

set redraw
"" }} Visual settings


"" Moving around, tabs, windows and so on {{
"" TODO: F4 still unassigned!

"Scroll the tabs
map <F5> :tabp<CR>
map <F6> :tabn<CR>

" Smart way to move between windows
 map <C-j> <C-W>j
 map <C-k> <C-W>k
 map <C-h> <C-W>h
 map <C-l> <C-W>l

"With spanish keyboard ^] for going to the tab is horribly painful
map <Leader>g <C-]>

"" Grep rules!!
""Grep -r in current directory
nnoremap <Leader>f :grep -r <cword> . <CR>

set wildmenu
"Mouse support
set mouse=a
map <ScrollWheelUp> <C-Y>
map <ScrollWheelDown> <C-E>

""}} Moving around


map <Leader>b :CtrlPBuffer<CR>

function! ToggleShowHiddenChars()
        :set listchars=eol:$,tab:>-,trail:~,extends:>,precedes:<
        :set list!
endfunction
let c_space_errors = 1
let python_space_error_highlight = 1
map <F8> /\s\+$

""There is an awesome script that loads some cscope mappings. Source it here
if !empty(glob("~/.vim/cscope_maps.vim"))
     source ~/.vim/cscope_maps.vim
endif

""nmap <C-y>s :cs find s <C-R>=expand("<cword>")<CR><CR>
nmap <Leader>cs :cs find s <C-R>=expand("<cword>")<CR><CR>
nmap <Leader>cg :cs find g <C-R>=expand("<cword>")<CR><CR>
"""nmap <Leader>cg :YcmCompleter GoTo <C-R>=expand("<cword>")<CR><CR>
"""nmap <Leader>ci :YcmCompleter GoToInclude <C-R>=expand("<cword>")<CR><CR>
"""nmap <Leader>cd :YcmCompleter GoToDeclalation <C-R>=expand("<cword>")<CR><CR>
"""nmap <Leader>cf :YcmCompleter GoToDefinition <C-R>=expand("<cword>")<CR><CR>
let g:ycm_global_ycm_extra_conf = '~/.vim/bundle/YouCompleteMe/third_party/ycm/.ycm_extra_conf.py'
 "" Encoding to utf-8 and NERDTree to use + and - instead of fancy arrows
set encoding=utf-8
 ""let g:NERDTreeDirArrows=0
let g:NERDTreeDirArrowExpandable = '*'
let g:NERDTreeDirArrowCollapsible = '+'


let g:tagbar_iconchars = ['*', '>']

""" YCM Config
"""let g:ycm_collect_identifiers_from_tags_files = 1 " Let YCM read tags from Ctags file
"""let g:ycm_use_ultisnips_completer = 1 " Default 1, just ensure
"""let g:ycm_seed_identifiers_with_syntax = 1 " Completion for programming language's keyword
"""let g:ycm_complete_in_comments = 1 " Completion in comments
"""let g:ycm_complete_in_strings = 1 " Completion in string
"""
"""let g:ycm_key_list_select_completion = ['<C-j>', '<Down>']
"""let g:ycm_key_list_previous_completion = ['<C-k>', '<Up>']

"""{{ Formatting
""" C/C++
Bundle 'rhysd/vim-clang-format'

let g:clang_format#style_options = {
            \ "BasedOnStyle": "LLVM",
            \ "BreakBeforeBraces": "BS_Linux",
            \ "ColumnLimit": 80,
            \ "IndentCaseLabels": "true",
            \ "PointerAlignment": "Left"}
" map to <Leader>a in C++ code
autocmd FileType c,cpp,hpp,h nnoremap <buffer><Leader>a :<C-u>ClangFormat<CR>
autocmd FileType c,cpp,hpp,h vnoremap <buffer><Leader>a :ClangFormat<CR>
autocmd FileType c,cpp,hpp,h let g:clang_format#auto_format = 1
let g:clang_format#detect_style_file = 1

" Toggle auto formatting:
nmap <Leader>C :ClangFormatAutoToggle<CR>
" Allow to auto format all code when save it. 0 false

""" YAML
autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab

""" No beeping please
set belloff=all

Bundle 'tomlion/vim-solidity'

Bundle 'vivien/vim-linux-coding-style'

"e	"}} End Formatting
